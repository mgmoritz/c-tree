#include <stdio.h>
#include <stdlib.h>

typedef struct treenode {
  int value;
  struct treenode *left;
  struct treenode *right;
} treenode;

treenode *create_node(int value) {
  treenode* result = malloc(sizeof(treenode));

  if (result != NULL) {
    result -> left = NULL;
    result -> right = NULL;
    result -> value = value;
  }
  return result;
}

void print_tabs(int numtabs) {
  for (int i = 0; i < numtabs; ++i) {
    printf("\t");
  }
}

void print_tree(treenode *root, int level) {
  if (root == NULL) {
    printf("-\n");
    return;
  }

  printf("%d\n", root->value);

  print_tabs(level + 1);
  printf("L: ");
  print_tree(root -> left, level + 1);

  print_tabs(level + 1);
  printf("R: ");
  print_tree(root -> right, level + 1);
}

int get_rank(treenode* node, int acc) {
  if (node == NULL) {
    return acc;
  }
  int leftRank = get_rank(node->left, acc + 1);
  int rightRank = get_rank(node->right, acc + 1);
  return leftRank > rightRank ? leftRank : rightRank;
}

int main(int argc, char *argv[])
{
  (void) argc;
  (void) argv;

  treenode* n1 = create_node(10);
  treenode* n2 = create_node(11);
  treenode* n3 = create_node(12);
  treenode* n4 = create_node(13);
  treenode* n5 = create_node(14);
  treenode* n6 = create_node(15);
  treenode* n7 = create_node(16);
  treenode* n8 = create_node(17);

  n1 -> left = n2;
  n1 -> right = n3;
  n3 -> left = n4;
  n3 -> right = n5;
  n4 -> left = n6;
  n6 -> left = n7;
  n6 -> right = n8;

  printf("rank: %d\n", get_rank(n1, 0));
  print_tree(n1, 0);

  free(n1);
  free(n2);
  free(n3);
  free(n4);
  free(n5);
  printf("%s\n", "Hello, World!");
  return 0;
}
